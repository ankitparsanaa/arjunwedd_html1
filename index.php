
<!DOCTYPE html>
<html lang="en-US" class="no-js">
    <head>
        <meta charset="UTF-8" />
        <title>Tying the Knot 10th December, 2014 | Arjun and Divya</title>
        <link rel="shortcut icon" href="/favicon.ico">
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="stylesheet" type="text/css" media="all" href="style.css" />

        <!--[if lt IE 9]>
        <script src="js/IE9.js"></script>
        <![endif]-->
        <!--[if lte IE 7]> <link href="css/ie.css" rel="stylesheet" type="text/css"><![endif]-->

        <script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js?ver=4.0'></script>
        <script type='text/javascript' src='js/jquery.scrollTo.min.js?ver=4.0'></script>
        <style>
            body {
                visibility: hidden;
            }
        </style>
        <script>
            jQuery(document).ready(function () {
                delay();
            });

            function delay() {
                var secs = 3000;
                setTimeout('initFadeIn()', secs);
            }

            function initFadeIn() {
                jQuery("body").css("visibility", "visible");
                jQuery("body").css("display", "none");
                jQuery("body").fadeIn(3000);
            }
        </script>
    </head>

    <body class="home blog">
        <div id="header">
            <div id="masthead">
                <div id="branding">
                    <a href="index.php" title="Arjun and Divya Home" rel="home"><img src="images/logo.png" alt="Arjun and Divya" /></a>
                </div>
                <div id="site_name">
                    Arjun and Divya
                </div>
                <div id="site_description">
                    Tying the Knot 10th December, 2014
                </div>
            </div><!--end #masthead -->
        </div><!-- #header -->
        <div id="wrapper">
            <div id="main">
                <div id="posts">
                    <div id="home-page">
                        <div>
                            <img class="" id="banner" src="images/arjundiv.jpg" style="width: 100%;height: 655px;">
                            <!-- <audio controls style="margin-top: -52px;position: absolute;right: 35px;">
                                    <source src="audio.mp3" type="audio/mpeg">
                            </audio> -->
                        </div>
                        <div>
                            <p style="color: #000;font-size: 40px;background-color: rgba(0,0,0,0.2);text-align:center;margin-top: -4px;">Je te aime</p>
                        </div>
                        <!-- <div class="content">
                                
                </div> -->
                    </div>
                    <!-- <div class="post_end">
                            
            </div> -->
                </div>
            </div><!--end #main -->

        </div><!--end #wrapper -->
        <div id="menu-main-menu-container" class="menu-main-menu-container">
            <ul id="menu-main-menu" class="menu">
                <li class="main-menu-item post-title-1"><a href="our-story.php" class="menu-link" >Our Story</a></li>
                <li class="main-menu-item post-title-2"><a href="family.php" class="menu-link" >Our Family</a></li>
                <li class="main-menu-item post-title-3"><a href="wedding-events.php" class="menu-link" >Wedding Events</a></li>
                <li class="main-menu-item post-title-4"><a href="photos.php" class="menu-link" >Photos</a></li>
                <li class="main-menu-item post-title-5"><a href="e-vite.php" class="menu-link" >E-vite</a></li>
                <li class="main-menu-item post-title-6"><a href="engagement.php" class="menu-link" >Engagement</a></li>
            </ul>
        </div>

        <div id="footer">
            <div id="footer_content">
                <div id="footer_icon"></div>
            </div>
        </div>
        <img class="loading_icon" src="images/loading.gif" />
        <link rel='stylesheet' id='vc_hotspot_cq_style-css'  href='css/vc/style.min.css?ver=4.0' type='text/css' media='all' />
        <link rel='stylesheet' id='tooltipster-css'  href='css/vc/tooltipster.css?ver=4.0' type='text/css' media='all' />
        <script type='text/javascript' src='js/vc/jquery.tooltipster.min.js?ver=4.0'></script>
        <script type='text/javascript' src='js/vc/script.min.js?ver=4.0'></script>
        <script>
            $(document).ready(function () {
                var audioElement = document.createElement('audio');
                audioElement.setAttribute('src', 'audio.mp3');
                //audioElement.setAttribute('autoplay', 'false');
                //audioElement.load()

                $.get();

                audioElement.addEventListener("load", function () {
                    audioElement.play();
                }, true);

                $('#banner').click(function () {
                    audioElement.play();
                });
            });
        </script> 
    </body>
</html>